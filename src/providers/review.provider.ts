import { Injectable } from '@angular/core';
import { Kanji } from '../app/classes/kanji';
import { BehaviorSubject, Observable } from 'rxjs';
import { KanjiDbProvider } from './db.provider';


@Injectable()
export class ReviewProvider {

  emptyKanji = new Kanji({
    character: '',
    meaning: '',
    hint: '',
    number: 0,
    reviewDate: "",
    interval: 0
  });

  // DB provider
  db: any;

  // Review 
  reviewSource: BehaviorSubject<number[]>;
  review: Observable<number[]>;

  // Current card 
  currentCardSource: BehaviorSubject<Kanji>;

  constructor( private kanjiDbProvider: KanjiDbProvider ) {
    // TO  DO Get kanjis an the Shuffle them and save it in the DB
    this.reviewSource = new BehaviorSubject([]);
    this.review = this.reviewSource.asObservable();

    this.currentCardSource = new BehaviorSubject(this.emptyKanji);

    this.kanjiDbProvider.getReviewKanjis().then( kanjis => {
      let kanjiArr: number[] = [];

      for (let i = 0; i < kanjis.length; i++) {
        kanjiArr.push(kanjis[i]);
      }
      console.log(kanjiArr);
      this.reviewSource.next(kanjiArr);

      this.kanjiDbProvider.getKanji(this.reviewSource.getValue()[0]).then( kanji => {
        console.log(kanji);
        this.currentCardSource.next(kanji);
      });
    });

    // Will inform when the review is empty no more items left
    this.db = kanjiDbProvider;
  }

  updateCards( answer: number ) {
    let review = this.reviewSource.getValue();
    let card = review.splice(0, 1)[0];

    if( answer == 0 ) {
      review.push(card);
    }

    if ( review[0] ) {
      this.db.getKanji( review[0] ).then( kanji => {
        this.currentCardSource.next(kanji);
        }); 
      this.reviewSource.next(review);
    }
    else {
      // Reset variables in order to have something in the Subject vars
      this.currentCardSource.next(this.emptyKanji);
      this.reviewSource.next([]);
    }
    // TODO update DB arr
  }

  setReviewDate( answer: number, currentInterval: number ) {
    let nextinterval: number;
    switch (answer) {
      case 0:
        nextinterval = 1;
        break;

      case 1:
        nextinterval = currentInterval + 1;
        break;

      case 2:
        nextinterval = currentInterval * 2;
        break;
    
      default:
        break;
    }
    return nextinterval;
  }
}
