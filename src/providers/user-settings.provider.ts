import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';


@Injectable()
export class UserSettingsProvider {

  constructor( public nativeStorage: NativeStorage ) {
    console.log('Hello UserSettingsProvider Provider');
  }

  getReview() {
    return this.nativeStorage.getItem('review').then( data => {
      if ( data.review.length > 0 ) {
        console.log("Review: " + data.review);
      }
      else {
        console.log("No Items in Review: " + data.review);
      }
      return data.review;
    },
    error => {
      let arr = [];
      this.nativeStorage.setItem( 'review', {review: arr} ).then(
        () => console.log('Stored review!'),
        error => console.error('Error storing item', JSON.stringify(error))
      );
      return arr;
    });
  }

}
