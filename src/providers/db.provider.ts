import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';


@Injectable()
export class KanjiDbProvider {
  private db: SQLiteObject;
  private options = { name: "kanji.sqlite3", location: 'default', createFromLocation: 1 };

  constructor(private sqlite: SQLite) {
    console.log('Hello KanjiDbProvider');
  }

  createDB() {
    this.sqlite.create(this.options).then((db: SQLiteObject) => {
      this.db = db;
    });
  }

  getReviewKanjis() {
    // TODO recieve parameter to use as LIMIT and to check that they dont have reviewDate
    return this.db.executeSql("SELECT * FROM kanjis LIMIT 3", []).then( data => {
      let kanjis = [];

      for (let i = 0; i < data.rows.length; i++) {
        kanjis.push( data.rows.item(i).number );
      }
      
      // Return the list of kanji ids to be reviewd
      return kanjis;
    })
    .catch(
      e => {
        console.log(JSON.stringify(e));
        return [];
      }
    );
  }

  getKanji( id: number ) {
    return this.db.executeSql(`SELECT * FROM kanjis WHERE number = ${ id }`, []).then(data => {
      // Return the kanji
      return data.rows.item(0);
    });
  }

}
