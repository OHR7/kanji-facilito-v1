import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Kanji } from '../app/classes/kanji';
import { ReviewProvider } from './review.provider';
import { KanjiDbProvider } from './db.provider';


@Injectable()
export class CurrentCardProvider {

  currentCard: Observable<Kanji>;
  currentCardSource: BehaviorSubject<Kanji>;

  constructor( public reviewProvider: ReviewProvider, private kanjiDbProvider: KanjiDbProvider ) {
    console.log('Hello CurrentCardProvider Provider');
    this.kanjiDbProvider.getKanji(this.reviewProvider.reviewSource.getValue()[0]).then( kanji => {
      this.currentCardSource = new BehaviorSubject(kanji);
    });
  }

}
