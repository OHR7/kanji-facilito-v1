import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Kanji } from '../../app/classes/kanji';
import { ReviewProvider } from '../../providers/review.provider';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
  selector: 'page-review',
  templateUrl: 'review.html',
})
export class ReviewPage {

  answer: boolean;
  currentCard: Kanji;
  notEmpty: number;

  review: any;

  constructor(
      public navCtrl: NavController, 
      public navParams: NavParams, 
      public reviewProvider: ReviewProvider,
    ) {
      this.answer = false;
  }

  ionViewDidLoad() {
    this.reviewProvider.review.subscribe(reviewArr => {
      this.notEmpty = reviewArr.length;
      if (reviewArr.length <= 0) {
        // go to results page
        this.navCtrl.setRoot(HomePage);
      }
    });
    this.reviewProvider.currentCardSource.subscribe(card => this.currentCard = card);
  }

  showAnswer() {
    this.answer = true;
  }

  answerKanji( answer: number ) {
    this.reviewProvider.updateCards(answer);
    this.answer = false;
  }

}
