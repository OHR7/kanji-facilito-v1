import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserSettingsProvider } from '../../providers/user-settings.provider';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  reviewStatus: boolean;

  constructor( public navCtrl: NavController, public _usp: UserSettingsProvider ) {
    this.checkReview();
  }

  goToPage() {
    this.navCtrl.push('ReviewPage');
  }

  checkReview() {
    this._usp.getReview().then( review => {
      console.log(review.length);
      if ( review.length > 0 ) {
        this.reviewStatus = true;
      } 
      else {
        this.reviewStatus = false;
      }
    })
  }

}
