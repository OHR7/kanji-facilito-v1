export class Kanji {
    character: string;
    meaning: string;
    hint: string;
    number: number;
    reviewDate: string;
    interval: number;
    
    constructor( object ) {
        this.character = object.character;
        this.meaning = object.meaning;
        this.hint = object.hint;
        this.number = object.number;
        this.reviewDate = object.reviewDate;
        this.interval = object.interval;
    }
}

export let cards: Kanji[] = [];

cards = [
    new Kanji({
        character: '力',
        meaning: 'Fuerza',
        hint: 'La repreentacion de la fuerza',
        number: 15,
        reviewDate: "",
        interval: 1
    }),
    new Kanji({
        character: '木',
        meaning: 'Arbol',
        hint: 'La repreentacion del arbol',
        number: 16,
        reviewDate: "",
        interval: 1
    }),
    new Kanji({
        character: '水',
        meaning: 'Agua',
        hint: 'La repreentacion de el agua',
        number: 17,
        reviewDate: "",
        interval: 1
    }),
];

export function initKanjis(): Kanji[] {
    return cards;
}